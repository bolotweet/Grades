<?php

/**
 * 

    Bolotweet-Grades
    Copyright (C) 2018  bolotweet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * Based on a development from Jorge J. Gomez-Sanz
 * and a template by Evan Prodromou
 * 
 *
 * @author   Alvaro Ortego <alvorteg@ucm.es>
 * @license  http://www.fsf.org/licensing/licenses/agpl.html AGPLv3
 *
 */
if (!defined('STATUSNET')) {
    exit(1);
}

class GradeexportcsvAction extends Action {

    var $user = null;

    /**
     * Take arguments for running
     *
     * This method is called first, and it lets the action class get
     * all its arguments and validate them. It's also the time
     * to fetch any relevant data from the database.
     *
     * Action classes should run parent::prepare($args) as the first
     * line of this method to make sure the default argument-processing
     * happens.
     *
     * @param array $args $_REQUEST args
     *
     * @return boolean success flag
     */
    function prepare(array $args = Array()) {
        parent::prepare($args);

        $this->user = common_current_user();

        return true;
    }

    /**
     * Handle request
     *
     * This is the main method for handling a request. Note that
     * most preparation should be done in the prepare() method;
     * by the time handle() is called the action should be
     * more or less ready to go.
     *
     * @param array $args $_REQUEST args; handled in prepare()
     *
     * @return void
     */
    function handle(array $args) {
        parent::handle($args);

        if (!common_logged_in()) {
             // TRANS: Not logged in.
            $this->clientError(_('Not logged in.'));
            return;
        }

        if (!$this->user->hasRole('grader')) {
            // TRANS: You have no privileges to visit this page.
            $this->clientError(_('You have no privileges to visit this page.'));
            return;
        }

        $groupid = $this->trimmed('groupid');
        $delimiter = $this->trimmed('grade-export-delimiter');
        $separator = $this->trimmed('grade-export-separator');

        $arrayReport = Grades::getGradedNoticesAndUsersWithinGroup($groupid);
        $nicksMembers = Grades::getMembersNicksExcludeGradersAndAdmin($groupid);

        foreach ($nicksMembers as $nick) {

            if (!array_key_exists($nick, $arrayReport)) {
                $arrayReport[$nick] = 0;
            }
        }

        $arrayFinal = array();

        foreach ($arrayReport as $alumno => $puntuacion) {
            $arrayFinal[] = array($alumno, number_format($puntuacion[0], 2),number_format($puntuacion[1], 2));
        }

        $this->generarInformeCSV($arrayFinal, 'report_group_' . $groupid . '.csv', $separator, $delimiter);
    }

    function generarInformeCSV($array, $filename = "report.csv", $separator = ',', $delimiter = '"') {

        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachement; filename="' . $filename . '";');

        if ($separator == "") {
            $separator = ',';
        }

        if ($delimiter == "") {
            $delimiter = '"';
        }


        $f = fopen('php://output', 'w');

        foreach ($array as $line) {
            fputcsv($f, $line, $separator, $delimiter);
        }
    }

    /**
     * Return true if read only.
     *
     * Some actions only read from the database; others read and write.
     * The simple database load-balancer built into StatusNet will
     * direct read-only actions to database mirrors (if they are configured),
     * and read-write actions to the master database.
     *
     * This defaults to false to avoid data integrity issues, but you
     * should make sure to overload it for performance gains.
     *
     * @param array $args other arguments, if RO/RW status depends on them.
     *
     * @return boolean is read only action?
     */
    function isReadOnly(array $args) {
        return false;
    }

}
