<?php

/**
 * 

    Bolotweet-Grades
    Copyright (C) 2018  bolotweet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * Based on a development from Jorge J. Gomez-Sanz
 * and a template by Evan Prodromou
 * 
 *
 * @author   Alvaro Ortego <alvorteg@ucm.es>
 * @license  http://www.fsf.org/licensing/licenses/agpl.html AGPLv3
 *
 */
if (!defined('STATUSNET') && !defined('LACONICA')) {
    exit(1);
}

/**
 * Make another user an grader of a group
 *
 */
class MakegraderAction extends RedirectingAction {

    var $profile = null;
    var $group = null;

    function prepare(array $args = Array()) {
        parent::prepare($args);
        if (!common_logged_in()) {
             // TRANS: error performing the action,not logged in
            $this->clientError(_('Not logged in.'));
            return false;
        }
        $id = $this->trimmed('profileid');
        if (empty($id)) {
            // TRANS: error performing the action, no profile specified
            $this->clientError(_('No profile specified.'));
            return false;
        }
        $this->profile = Profile::getKV('id', $id);
        if (empty($this->profile)) {
            // TRANS: error performing the action,No profile with that ID.
            $this->clientError(_('No profile with that ID.'));
            return false;
        }
        $group_id = $this->trimmed('groupid');
        if (empty($group_id)) {
             // TRANS: error performing the action,No group specified.
            $this->clientError(_('No group specified.'));
            return false;
        }
        $this->group = User_group::getKV('id', $group_id);
        if (empty($this->group)) {
            
            $this->clientError(_('No such group.'));
            return false;
        }
        $user = common_current_user();
        if (!$user->isAdmin($this->group) &&
                !$user->hasRole('grader')) {
             // TRANS: error performing the action, Only an admin and grader can make another user a grader
            $this->clientError(_('Only an admin and grader can make another user a grader.'), 401);
            return false;
        }
        if ($this->profile->hasRole('grader')) {
             // TRANS: error performing the action, %1$s is already a grader for group "%2$s".'
            $this->clientError(sprintf(_('%1$s is already a grader for group "%2$s".'), $this->profile->getBestName(), $this->group->getBestName()), 401);
            return false;
        }
        return true;
    }

    /**
     * Handle request
     *
     * @param array $args $_REQUEST args; handled in prepare()
     *
     * @return void
     */
    function handle(array $args) {
        parent::handle($args);
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->makeGrader();
        }
    }

    /**
     * Make user an admin
     *
     * @return void
     */
    function makeGrader() {

        if (!$this->profile->hasRole('grader')) {
            $this->profile->grantRole('grader');
        }

        if (!$this->profile->hasRole('deleter')) {
            $this->profile->grantRole('deleter');
        }

        $result = Gradesgroup::vincularGrupo($this->profile->id, $this->group->id);

        if (!$result) {
// TRANS: error performing the action, There has been a mistake while binding %1$s to group "%2$s".
            $this->clientError(sprintf(_('There has been a mistake while binding %1$s to group "%2$s"'), $this->profile->getBestName(), $this->group->getBestName()), 401);
        }

        $this->returnToPrevious();
    }

    /**
     * If we reached this form without returnto arguments, default to
     * the top of the group's member list.
     * 
     * @return string URL
     */
    function defaultReturnTo() {
        return common_local_url('groupmembers', array('nickname' => $this->group->nickname));
    }

}
