<?php

/**
 * 
   Bolotweet-Grades
    Copyright (C) 2018  bolotweet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * Based on a development from Jorge J. Gomez-Sanz
 * and a template by Evan Prodromou
 * 
 *
 * @author   Alvaro Ortego <alvorteg@ucm.es>
 * @license  http://www.fsf.org/licensing/licenses/agpl.html AGPLv3
 *
 */
if (!defined('STATUSNET')) {
    exit(1);
}

class GradereportAction extends Action {

    var $user = null;

    /**
     * Take arguments for running
     *
     * This method is called first, and it lets the action class get
     * all its arguments and validate them. It's also the time
     * to fetch any relevant data from the database.
     *
     * Action classes should run parent::prepare($args) as the first
     * line of this method to make sure the default argument-processing
     * happens.
     *
     * @param array $args $_REQUEST args
     *
     * @return boolean success flag
     */
    function prepare(array $args = Array()) {
        parent::prepare($args);

        $this->user = common_current_user();

        return true;
    }

    /**
     * Handle request
     *
     * This is the main method for handling a request. Note that
     * most preparation should be done in the prepare() method;
     * by the time handle() is called the action should be
     * more or less ready to go.
     *
     * @param array $args $_REQUEST args; handled in prepare()
     *
     * @return void
     */
    function handle() {
        parent::handle();

        if (!common_logged_in()) {
            $this->clientError(_('Not logged in.'));
            return;
        }

        $this->showPage();
    }

    /**
     * Title of this page
     *
     * Override this method to show a custom title.
     *
     * @return string Title of the page
     */
    function title() {
        // TRANS: grade report title
        return _('Grade Reports');
    }

    /**
     * Show content in the content area
     *
     * The default StatusNet page has a lot of decorations: menus,
     * logos, tabs, all that jazz. This method is used to show
     * content in the content area of the page; it's the main
     * thing you want to overload.
     *
     * This method also demonstrates use of a plural localized string.
     *
     * @return void
     */
    function showContent() {
        if (empty($this->user)) {
            $this->element('p', array('class' => 'grade-report-error'), _m('Login first!'));
        } else {

            if ($this->user->hasRole('grader')) {
                $this->showReportGrader();
            } else {
                $this->showReportNoGrader();
            }
        }
    }

    function showReportNoGrader() {
        common_log(LOG_ERR, "gr0");
        $groupsUser = $this->user->getGroups()->fetchAll();

        if (empty($groupsUser)) {
            // TRANS: the report requester does not belong to any group
            $this->element('p', null, _('You still don\'t belong to any group.'));
        }

        foreach ($groupsUser as $group) {
            $gradespergroup = Grades::getGradedNoticesAndUsersWithinGroup($group->id);
            $nicksMembers = Grades::getMembersNicksExcludeGradersAndAdmin($group->id);

            foreach ($nicksMembers as $nick) {
                if (!array_key_exists($nick, $gradespergroup)) {
                    $gradespergroup[$nick] = '0';
                }
            }

            $this->elementStart('div', array('id' => 'grade-report-group-' . $group->id, 'class' => 'div-group-report'));
            $this->elementStart('h3', array('class' => 'grade-report-group', 'title' => $group->getBestName()));
            $this->element('a', array('class' => 'grade-report-group-link', 'href' =>
                common_root_url() . 'group/' . $group->nickname), $group->getBestName());
            $this->elementEnd('h3');
            $this->element('a', array('class' => 'grade-show-report', 'href' =>
                'javascript:mostrarReport(' . $group->id . ',"' . _m('Expandir') . '","' . _m('Ocultar') . '");'), _m('Expandir'));

            $this->element('p', array('class' => 'grade-reports-group-underline'), '');

            $this->elementStart('div', array('class' => 'report-group-hidden'));
            common_log(LOG_ERR, "gr1");
            if (empty($gradespergroup))
            // TRANS: there are still no scores
                $this->element('p', null, _('There are still no scores'));

            else {
                $this->elementStart('ol', array('class' => 'grade-report-groupmembers'));
                $gradessize = count($gradespergroup);
                $gradescounter = 0;
                common_log(LOG_ERR, "gr1");
                foreach ($gradespergroup as $alumno => $puntuacion) {
                    $gradescounter++;
                    $avatar = NULL;
                    $this->elementStart('li', array('class' => 'grade-report-groupmembers-item'));
                    $profile = Profile::getKV('nickname', $alumno);
                    common_log(LOG_ERR, "gr3");
                    try {
                        $avatar = $profile->getAvatar(AVATAR_MINI_SIZE);
                    } catch (NoAvatarException $e) {
                        
                    };
                    common_log(LOG_ERR, "gr4");

                    if ($avatar) {
                        common_log(LOG_ERR, "gr5");
                        $avatar = $avatar->displayUrl();
                        common_log(LOG_ERR, "gr51");
                    } else {
                        common_log(LOG_ERR, "gr6");
                        $avatar = Avatar::defaultImage(AVATAR_MINI_SIZE);
                        common_log(LOG_ERR, "gr61");
                    }
                    if ($gradescounter <= ($gradessize * 0.25) || $profile->id == $this->user->id) {
                        $this->element('img', array('src' => $avatar));
                        $this->elementStart('a', array('class' => 'user-link-report',
                            'href' => common_local_url('gradeshowuser', array('nickgroup' => $group->nickname, 'nickname' => $profile->nickname))));
                    } else {
                        $this->element('img', array('src' => "/bolosocial/local/plugins/Grades/css/buggi-anonymous.png"));
                    }

                    if ($profile->id == $this->user->id) {
                        $this->raw($profile->getBestName() . ', ' . number_format($puntuacion[0], 2) . " avg:" . number_format(floatval($puntuacion[1]), 2));
                        $this->elementEnd('a');
                    } else
                    if ($gradescounter <= ($gradessize * 0.25)) {
                        $this->raw($profile->getBestName());
                        $this->elementEnd('a');
                    } else
                        $this->raw(" ");

                    $this->elementEnd('li');
                }
                $this->elementEnd('ol');
            }
            $this->elementEnd('div');
            $this->elementEnd('div');
        }
    }

    function showReportGrader() {

        $groupsUser = $this->user->getGroups()->fetchAll();


        if (empty($groupsUser)) {
            // TRANS: the report requester does not belong to any group
            $this->element('p', null, _('You still don\'t belong to any group.'));
        }

        foreach ($groupsUser as $group) {
            $gradespergroup = Grades::getGradedNoticesAndUsersWithinGroup($group->id);
            $nicksMembers = Grades::getMembersNicksExcludeGradersAndAdmin($group->id);

            foreach ($nicksMembers as $nick) {

                if (!array_key_exists($nick, $gradespergroup)) {
                    $gradespergroup[$nick] = array('0', '0');
                }
            }

            $this->elementStart('div', array('id' => 'grade-report-group-' . $group->id, 'class' => 'div-group-report'));
            $this->elementStart('h3', array('class' => 'grade-report-group', 'title' => $group->getBestName()));
            $this->element('a', array('class' => 'grade-report-group-link', 'href' =>
                common_root_url() . 'group/' . $group->nickname), $group->getBestName());
            $this->elementEnd('h3');

            $this->element('a', array('class' => 'grade-show-report', 'href' =>
                'javascript:mostrarReport(' . $group->id . ',"' . _m('Expandir') . '","' . _m('Ocultar') . '");'), _m('Expandir'));
            $this->element('a', array('class' => 'grade-export-report', 'href' => common_local_url('gradeoptionscsv') . '?group=' . $group->id), _m('Exportar CSV'));

            $this->element('p', array('class' => 'grade-reports-group-underline'), '');

            $this->elementStart('div', array('class' => 'report-group-hidden'));
            if (empty($gradespergroup))
            // TRANS: there are still no scores
                $this->element('p', null, _('There are still no scores'));

            else {
                $this->elementStart('ol', array('class' => 'grade-report-groupmembers'));
                //  $this->raw(print_r($gradespergroup,true));
                foreach ($gradespergroup as $alumno => $puntuacion) {

                    $this->elementStart('li', array('class' => 'grade-report-groupmembers-item'));
                    $profile = Profile::getKV('nickname', $alumno);
                    $avatar = NULL;
                    try {
                        $avatar = $profile->getAvatar(AVATAR_MINI_SIZE);
                    } catch (NoAvatarException $e) {
                        
                    };
                    if ($avatar) {
                        $avatar = $avatar->displayUrl();
                    } else {
                        $avatar = Avatar::defaultImage(AVATAR_MINI_SIZE);
                    }
                    $this->element('img', array('src' => $avatar));
                    $this->elementStart('a', array('class' => 'user-link-report', 'href' => common_local_url('gradeshowuser', array('nickgroup' => $group->nickname, 'nickname' => $profile->nickname))));
                    // $this->raw(print_r($puntuacion[1],true));
                    $this->raw($profile->getBestName());
                    try {
                        // $this->raw(print_r($puntuacion,true));

                        $this->raw(', total:' . number_format($puntuacion[0], 2) . " avg:" . number_format(floatval($puntuacion[1]), 2));
                    } catch (Exception $e) {
                        
                    };
                    $this->elementEnd('a');
                    $this->elementEnd('li');
                }
                $this->elementEnd('ol');
            }
            $this->elementEnd('div');
            $this->elementEnd('div');
        }
    }

    /**
     * Return true if read only.
     *
     * Some actions only read from the database; others read and write.
     * The simple database load-balancer built into StatusNet will
     * direct read-only actions to database mirrors (if they are configured),
     * and read-write actions to the master database.
     *
     * This defaults to false to avoid data integrity issues, but you
     * should make sure to overload it for performance gains.
     *
     * @param array $args other arguments, if RO/RW status depends on them.
     *
     * @return boolean is read only action?
     */
    function isReadOnly($args) {
        return false;
    }

}
