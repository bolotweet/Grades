<?php

/**
 * 

    Bolotweet-Grades
    Copyright (C) 2018  bolotweet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * Based on a development from Jorge J. Gomez-Sanz
 * and a template by Evan Prodromou
 * 
 *
 * @author   Alvaro Ortego <alvorteg@ucm.es>
 * @license  http://www.fsf.org/licensing/licenses/agpl.html AGPLv3
 *
 */
if (!defined('STATUSNET')) {
    exit(1);
}

class GradeoptionscsvAction extends Action {

    var $user = null;
    var $error = null;

    /**
     * Take arguments for running
     *
     * This method is called first, and it lets the action class get
     * all its arguments and validate them. It's also the time
     * to fetch any relevant data from the database.
     *
     * Action classes should run parent::prepare($args) as the first
     * line of this method to make sure the default argument-processing
     * happens.
     *
     * @param array $args $_REQUEST args
     *
     * @return boolean success flag
     */
    function prepare(array $args = Array()) {
        parent::prepare($args);

        $this->user = common_current_user();

        return true;
    }

    /**
     * Handle request
     *
     * This is the main method for handling a request. Note that
     * most preparation should be done in the prepare() method;
     * by the time handle() is called the action should be
     * more or less ready to go.
     *
     * @param array $args $_REQUEST args; handled in prepare()
     *
     * @return void
     */
    function handle($args) {
        parent::handle($args);

        if (!common_logged_in()) {
            // TRANS: error performing the action,not logged in
            $this->clientError(_('Not logged in.'));
            return;
        }

        if (!$this->user->hasRole('grader')) {
             // TRANS: error performing the action, you don't have the privileges to visit this page
            $this->clientError(_('You don\'t have the privileges to visit this page'));
            return;
        }


        if (empty($_GET['group'])) {
            // TRANS: error performing the action, the associated group is missing
            $this->showForm(_("The associated group is missing"));
            return;
        } else {
            $this->showForm();
        }
    }

    function showContent() {
        if (empty($this->user)) {
            $this->element('p', array('class' => 'grade-report-error'), _m('Login first!'));
        } else if ($this->error) {

            $this->elementStart('p');
              // TRANS: error performing the action, there has been an error when accesing the link
            $this->raw(sprintf(_('There has been a mistake while accesing this link.Try again to access %s Grade Reports %s, and try again.'),sprintf("<a href=%s>",common_local_url('gradereport')),"</a>"));
            $this->elementEnd('p');
        } else {
            // TRANS: the report can be customized with the scores
            $this->element('p', null, _('Following, you will be able to customize the report with the scores'));
            $this->element('br');

            $groupid = $_GET['group'];

            $formExport = new GradecsvForm($this, $groupid);
            $formExport->show();
        }
    }

    function showForm($error = null) {
        $this->error = $error;
        $this->showPage();
    }

    function showPageNotice() {
        if ($this->error) {
             // TRANS: detected an error
            $this->element('p', _('error'), $this->error);
        }
    }

    /**
     * Title of this page
     *
     * Override this method to show a custom title.
     *
     * @return string Title of the page
     */
    function title() {
         // TRANS: Options for exporting a CSV file
        return _('CSV export options');
    }

    /**
     * Return true if read only.
     *
     * Some actions only read from the database; others read and write.
     * The simple database load-balancer built into StatusNet will
     * direct read-only actions to database mirrors (if they are configured),
     * and read-write actions to the master database.
     *
     * This defaults to false to avoid data integrity issues, but you
     * should make sure to overload it for performance gains.
     *
     * @param array $args other arguments, if RO/RW status depends on them.
     *
     * @return boolean is read only action?
     */
    function isReadOnly($args) {
        return false;
    }

}
