<?php

/**
 * 
    Bolotweet-Grades
    Copyright (C) 2018  bolotweet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 

   Based on a development from Jorge J. Gomez-Sanz
 * and a template by Evan Prodromou
 * 
 *
 * @author   Alvaro Ortego <alvorteg@ucm.es>
 * @license  http://www.fsf.org/licensing/licenses/agpl.html AGPLv3
 *
 */
class GradeshowuserAction extends Action {

    var $alumno = null;
    var $group = null;
    var $page = null;
    var $notice = null;
    var $error = null;
    // Variables para estadísticas

    var $numeroTweets = null;
    var $notaMedia = null;
    var $notaTotal = null;
    var $numeroTweetsPuntuados = null;

    /**
     * Take arguments for running
     *
     * This method is called first, and it lets the action class get
     * all its arguments and validate them. It's also the time
     * to fetch any relevant data from the database.
     *
     * Action classes should run parent::prepare($args) as the first
     * line of this method to make sure the default argument-processing
     * happens.
     *
     * @param array $args $_REQUEST args
     *
     * @return boolean success flag
     */
    function prepare(array $args = Array()) {
        parent::prepare($args);

        $alumno = $this->trimmed('nickname');
        $group = $this->trimmed('nickgroup');

        $this->group = User_group::getKV('nickname', $group);
        $this->alumno = Profile::getKV('nickname', $alumno);

        $this->page = ($this->arg('page')) ? ($this->arg('page') + 0) : 1;

        $ids = Grades::getNoticeFromUserInGroup($this->alumno->id, $this->group->id);

        $this->generarEstadisticas($ids);

        $this->notice = $this->getNotices(($this->page - 1) * NOTICES_PER_PAGE, NOTICES_PER_PAGE + 1, $ids);

        common_set_returnto($this->selfUrl());

        return true;
    }

    function showPageNotice() {
        if ($this->error) {
            $this->element('p', 'error', $this->error);
        }
    }

    function getNotices($offset, $limit, $ids) {

        if (empty($ids)) {
            // TRANS: the user has not published anything yet in the group
            $this->error = sprintf(_("The user %s  has not published anything in group %s"),$this->alumno->nickname,$this->group->nickname);
            return;
        }

        $total = $offset + $limit;
        $idsFinal=array();
        for ($i = $offset; $i < $total && $i<count($ids); $i++) {
            $idsFinal[] = $ids[$i];
        }

        $notices = Notice::multiGet('id', $idsFinal);

        return $notices;
    }

    function title() {

        if ($this->page == 1) {
            // TRANS: Page title for first group page. %s is a group name.
            return sprintf(_('Scored tweets from %s in %s'), $this->alumno->nickname, strtoupper($this->group->nickname));
        } else {
            // TRANS: Page title for any but first group page.
            // TRANS: %1$s is a group name, $2$s is a page number.
            return sprintf(_('Scored tweets from %1$s en %2$s (%3$d)'), $this->alumno->nickname, strtoupper($this->group->nickname), $this->page);
        }
    }

    /**
     * Handle request
     *
     * This is the main method for handling a request. Note that
     * most preparation should be done in the prepare() method;
     * by the time handle() is called the action should be
     * more or less ready to go.
     *
     * @param array $args $_REQUEST args; handled in prepare()
     *
     * @return void
     */
    function handle(array $args) {
        parent::handle($args);

        $this->showPage();
    }

    function showContent() {
        $this->showUserNotices();
    }

    function showUserNotices() {

        if ($this->error) {
            $this->elementStart('p');
            // TRANS: Link to return to grade reports page
            $this->raw(sprintf(_('Return to %s Grade Reports %s.'),sprintf("<a href=%s >",common_local_url('gradereport')),"</a>" ));
            $this->elementEnd('p');
        } else {
            $nl = new NoticeList($this->notice, $this);
            $cnt = $nl->show();
            $this->pagination($this->page > 1, $cnt > NOTICES_PER_PAGE, $this->page, 'gradeshowuser', array('nickgroup' => $this->group->nickname, 'nickname' => $this->alumno->nickname));
        }
    }

    function showProfileBlock() {
        $block = new AccountProfileBlock($this, $this->alumno);
        $block->show();
    }

    function showSections() {
        $user = common_current_user();

        parent::showSections();

        if ($user->hasRole('grader')) {
            $this->showStatistics();
        }
    }

    function showStatistics() {

        $this->elementStart('div', array('id' => 'entity_statistics',
            'class' => 'section'));

        // TRANS: H2 text for user statistics.
        $this->element('h2', null, _('Stats'));

        $this->elementStart('p');
        // TRANS: number of tweets
        $this->raw(_('Number of Tweets: '));
        $this->elementStart('span', array('class' => 'statistics-span'));
        $this->raw($this->numeroTweets);
        $this->elementEnd('span');
        $this->elementEnd('p');

        $this->elementStart('p');
        // TRANS: scored tweets.
        $this->raw(_('Tweets Puntuados: '));
        $this->elementStart('span', array('class' => 'statistics-span'));
        $this->raw($this->numeroTweetsPuntuados);
        $this->elementEnd('span');
        $this->elementEnd('p');

        $this->elementStart('p');
        // TRANS: average score.
        $this->raw('Average score: ');
        $this->elementStart('span', array('class' => 'statistics-span'));
        $this->raw($this->notaMedia);
        $this->elementEnd('span');
        $this->elementEnd('p');

        $this->elementStart('p');
        // TRANS: total score.
        $this->raw('Total Score: ');
        $this->elementStart('span', array('class' => 'statistics-span'));
        $this->raw($this->notaTotal);
        $this->elementEnd('span');
        $this->elementEnd('p');

        $this->elementEnd('div');
    }

    function isReadOnly($args) {
        return false;
    }

    function generarEstadisticas($ids) {

        $this->numeroTweetsPuntuados = count($ids);
        $this->numeroTweets = Grades::getNumberTweetsOfUserInGroup($this->alumno->id, $this->group->id);
        $notas = Grades::getNotaMediaYTotalofUserinGroup($this->alumno->id, $this->group->id);

        $this->notaMedia = number_format(reset($notas), 2);
        $this->notaTotal = number_format(key($notas), 2);

        if (empty($this->notaTotal))
            $this->notaTotal = number_format(0, 2);
    }

}
