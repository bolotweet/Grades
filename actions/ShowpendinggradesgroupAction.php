<?php

/**
 * 
    Bolotweet-Grades
    Copyright (C) 2018  Jorge J. Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
   Based on a development from Jorge J. Gomez-Sanz
 * and an original source by Evan Prodromou
 * 
 *
 * @author   Jorge J. Gomez-Sanz <jjgomez@ucm.es>
 * @license  http://www.fsf.org/licensing/licenses/agpl.html AGPLv3
 *
 */
if (!defined('STATUSNET') && !defined('LACONICA')) {
    exit(1);
}

require_once INSTALLDIR . '/lib/noticelist.php';
require_once INSTALLDIR . '/lib/feedlist.php';

/**
 * Group main page
 *
 * @category Group
 * @package  StatusNet
 * @author   Evan Prodromou <evan@status.net>
 * @license  http://www.fsf.org/licensing/licenses/agpl-3.0.html GNU Affero General Public License version 3.0
 * @link     http://status.net/
 */
class ShowpendinggradesgroupAction extends ShowgroupAction {

    /** page we're viewing. */


    /**
     * Is this page read-only?
     *
     * @return boolean true
     */
    function isReadOnly(array $args) {
        return true;
    }

    /**
     * Title of the page
     *
     * @return string page title, with page number
     */
    function title() {
        $base = $this->group->getFancyName();

        if ($this->page == 1) {
            // TRANS: Page title for first group page. %s is a group name.
            return sprintf(_('%s group, pending grades %d'), $base,Gradesgroup::getPending($base));
        } else {
            // TRANS: Page title for any but first group page.
            // TRANS: %1$s is a group name, $2$s is a page number.
            return sprintf(_('%1$s group,  pending grades %d, page %2$d'), $base, Gradesgroup::getPending($base), $this->page);
        }
    }

    /**
     * Prepare the action
     *
     * Reads and validates arguments and instantiates the attributes.
     *
     * @param array $args $_REQUEST args
     *
     * @return boolean success flag
     */
    function prepare(array $args = Array()) {
        parent::prepare($args);

        $this->page = ($this->arg('page')) ? ($this->arg('page') + 0) : 1;

        $this->userProfile = Profile::current();

        $user = common_current_user();

        if (!empty($user) && $user->streamModeOnly()) {
            $stream = new PendingGradesGroupNoticeStream($this->group, $this->userProfile);
        } else {
            $stream = new PendingGradesThreadingGroupNoticeStream($this->group, $this->userProfile);
        }

        $this->notice = $stream->getNotices(
                ($this->page - 1) * NOTICES_PER_PAGE*2, 
                NOTICES_PER_PAGE*2 + 1);

        common_set_returnto($this->selfUrl());

        return true;
    }

    /**
     * Handle the request
     *
     * Shows a profile for the group, some controls, and a list of
     * group notices.
     *
     * @return void
     */
    function handle(array $args) {
        $this->showPage();
    }

    /**
     * Show the page content
     *
     * Shows a group profile and a list of group notices
     */
    function showContent() {
        $this->showGroupNotices();
    }

    /**
     * Show the group notices
     *
     * @return void
     */
    function showGroupNotices() {
        $user = common_current_user();

        if (!empty($user) && $user->streamModeOnly() && $this->group->force_scope &&
                (empty($this->userProfile) || !$this->userProfile->isMember($this->group))) {

            $this->elementStart('p');
            $this->raw('Este grupo es privado, por lo que no es posible ver su contenido.<br/><br/>'
                    . 'Si lo desea, puede comunicarse con el administrador del grupo, o solicitar unirse al grupo. ');
            $this->elementEnd('p');
        } else {

         /*   if (empty($this->notice->_items) ) {

                $this->elementStart('p');
                $this->raw('¡Una lástima! Este grupo aún no tiene mensajes. ');
                $this->elementEnd('p');
            } else {*/
                
                if (!empty($user) && $user->streamModeOnly()) {
                    $nl = new PrimaryNoticeList($this->notice, $this, array('show_n'=>NOTICES_PER_PAGE*2));
                } else {
                    $nl = new ThreadedNoticeList($this->notice, $this, $this->userProfile);
                }

                $cnt = $nl->show();


                $this->pagination($this->page > 1, $cnt > NOTICES_PER_PAGE*2, 
                        $this->page, 'showpendinggradesgroup', 
                        array('nickname' => $this->group->nickname));
            //}
        }
    }



}

class PendingGradesThreadingGroupNoticeStream extends ThreadingNoticeStream
{
    function __construct($group, $profile)
    {
        parent::__construct(new PendingGradesGroupNoticeStream($group, $profile));
    }
}
