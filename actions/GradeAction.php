<?php

/**
 * 
    Bolotweet-Grades
    Copyright (C) 2018  bolotweet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


 * Based on a development from Jorge J. Gomez-Sanz
 * and a template by Evan Prodromou
 * 
 *
 * @author   Alvaro Ortego <alvorteg@ucm.es>
 * @license  http://www.fsf.org/licensing/licenses/agpl.html AGPLv3
 *
 */
if (!defined('STATUSNET')) {
    exit(1);
}

/**
 * Give a warm greeting to our friendly user
 *
 * This sample action shows some basic ways of doing output in an action
 * class.
 *
 * Action classes have several output methods that they override from
 * the parent class.
 *
 * @category Sample
 * @package  StatusNet
 * @author   Evan Prodromou <evan@status.net>
 * @license  http://www.fsf.org/licensing/licenses/agpl.html AGPLv3
 * @link     http://status.net/
 */
require_once INSTALLDIR . '/classes/User.php';
require_once INSTALLDIR . '/classes/Notice.php';

class GradeAction extends Action {

    var $user = null;

    /**
     * Take arguments for running
     *
     * This method is called first, and it lets the action class get
     * all its arguments and validate them. It's also the time
     * to fetch any relevant data from the database.
     *
     * Action classes should run parent::prepare($args) as the first
     * line of this method to make sure the default argument-processing
     * happens.
     *
     * @param array $args $_REQUEST args
     *
     * @return boolean success flag
     */
    function prepare(array $args = Array()) {
        parent::prepare($args);

        $this->user = common_current_user();


        return true;
    }

    /**
     * Class handler.
     *
     * @param array $args query arguments
     *
     * @return void
     */
    function handle() {

        parent::handle();
        if (!common_logged_in()) {
             // TRANS: error while performing action, Not logged in
            $this->clientError(_('Not logged in.'));
            return;
        }
        $user = common_current_user();
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            common_redirect(common_local_url('all', array('nickname' => $user->nickname)));
            return;
        }
        $noticeid = $this->trimmed('notice');
        $notice = Notice::getKV('id',$noticeid);


        $token = $this->trimmed('token-' . $notice->id);
        common_log(LOG_ERR,$noticeid);
        if (!$token || $token != common_session_token()) {
            // TRANS:  error while performing action, there was a problem with your session token. Try again, please.
            $this->clientError(_('There was a problem with your session token. Try again, please.'));
            return;
        }

        $gradevalue = $this->trimmed('value');
        if (!isset($gradevalue) || !in_array(intval($gradevalue),array(1,2,3,4), true)){
                 $this->clientError(_('Invalid grade value. Try again, please.'));
            return;
        }
        $nickname = $user->nickname;

        $exist = Grades::getNoticeGrade($noticeid, $nickname);

        if ($exist != '?') {

            Grades::updateNotice(array('noticeid' => $noticeid,
                'grade' => $gradevalue, 'userid' => $nickname));
        } else {
            Grades::register(array('userid' => $nickname,
                'noticeid' => $noticeid,
                'grade' => $gradevalue));
        }


        if ($this->boolean('ajax')) {

            $this->startHTML('application/xml,text/xml;charset=utf-8');
            $this->elementStart('head');
             // TRANS: disfavor a favourite
            $this->element('title', null, _('Disfavor favorite'));
            $this->elementEnd('head');
            $this->elementStart('body');
            $this->element('p');
            $this->elementEnd('body');
            $this->elementEnd('html');
        }
    }

 
}
