<?php

/**
 * 
 *  Bolotweet-Grades
    Copyright (C) 2018  bolotweet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * Based on a development from Jorge J. Gomez-Sanz
 * and a template by Evan Prodromou
 * 
 *
 * @author   Alvaro Ortego <alvorteg@ucm.es>
 * @license  http://www.fsf.org/licensing/licenses/agpl.html AGPLv3
 *
 */
if (!defined('STATUSNET')) {
    exit(1);
}

require_once INSTALLDIR . '/lib/util.php';

class GradesPlugin extends Plugin {

    public $showMarkers;

    /**
     * constructor
     */
    function __construct($show = TRUE) {
        $this->showMarkers = $show;
        parent::__construct();
    }

    function onCheckSchema() {
        $schema = Schema::get();

        $schema->ensureTable('grades', Grades::schemaDef());
        $schema->ensureTable('grades_group', Gradesgroup::schemaDef());

        return true;
    }

    function onInitializePlugin() {
        // A chance to initialize a plugin in a complete environment
    }

    function onCleanupPlugin() {
        // A chance to cleanup a plugin at the end of a program.
    }

    function onRouterInitialized($m) {
        $m->connect('group/:nickname/showpending', array('action' => 'showpendinggradesgroup'),
                array( 'nickname' => Nickname::DISPLAY_FMT));
        $m->connect('main/grade', array('action' => 'grade'));
        $m->connect('main/gradereport', array('action' => 'gradereport'));
        $m->connect('main/exportCSV/generate', array('action' => 'gradeexportcsv'));
        $m->connect('main/exportCSV/options', array('action' => 'gradeoptionscsv'));
        $m->connect('main/gradereport/:nickgroup/:nickname', array('action' => 'gradeshowuser'), 
                array('nickgroup' => Nickname::DISPLAY_FMT, 'nickname' => Nickname::DISPLAY_FMT));
        $m->connect('group/:nickname/makegrader', array('action' => 'makegrader'), array('nickname' => Nickname::DISPLAY_FMT));
        return true;
    }

    function onPluginVersion(array &$versions) {
        $versions[] = array('name' => 'Grade',
            'version' => 1.0,
            'author' => 'Alvaro Ortego', 'Jorge Gomez',
            'rawdescription' =>
            _m('A plugin to grade notices'));
        return true;
    }

    function onAutoload($cls) {

        $dir = dirname(__FILE__);

        switch ($cls) {
            case 'ShowpendinggradesgroupAction':
            case 'GradeAction':
            case 'GradereportAction':
            case 'GradeexportcsvAction':
            case 'GradeoptionscsvAction':
            case 'GradeshowuserAction':
            case 'MakegraderAction':
                include_once $dir . '/actions/' . $cls . '.php';
                return false;
            case 'GradeForm':
            case 'GradecsvForm':
            case 'MakeGraderForm':
            case 'PendingGradesGroupNoticeStream':
            case 'PendingGradesThreadingGroupNoticeStream':
                include_once $dir . '/lib/' . $cls . '.php';
                return false;
            case 'Grades':
            case 'Gradesgroup':
                include_once $dir . '/classes/' . $cls . '.php';
                return false;
                break;
            default:
                return true;
        }
    }

    function showNumbers($args, $value) {

        $grade = new GradeForm($args->out, $args->notice, $value);
        $grade->show();
    }

    function onEndToolsLocalNav($action) {
        // '''common_local_url()''' gets the correct URL for the action name we provide
        $user = common_current_user();
         $actionName = $action->trimmed('action');
        if (!empty($user)) {        
             if ($actionName === 'gradereport')
                 $action->elementStart('li', array('id' => 'nav_task', 'class' => 'current'));
            else
                $action->elementStart('li', array('id' => 'nav_task'));
           
              $action->elementStart('a', array('href' => common_local_url('gradereport'), 'title' =>  _m('Reports on user behaviors ')));
            $action->raw(_m('Grade Reports'));
            $action->elementEnd('a');
            $action->elementEnd('li');
            
              
        }

        return true;
    }

    function onEndShowGroupProfileBlock($out, $group) {

        $user = common_current_user();

        if (!empty($user)) {
            if ($user->hasRole('grader')) {    
                $out->elementStart('div',array('id'=>"pending-grades",'class'=> "section"));
                $out->elementStart('a', array('href' =>   common_local_url('showpendinggradesgroup',array( 'nickname' => $group->nickname)),
                    'class' => 'pendinggradeslink', 'id' => 'pending-gradeslink-' . $group->nickname, 'title' => _('Pending grades')));
                $out->elementStart("span", array('class'=>"pending-grades"));
                 $out->text(_m('Pending:'));
                   $out->elementEnd("span");
                 $out->elementStart("span", array('class'=>"pending-grades-number"));
                 $out->text(Gradesgroup::getPending($group->nickname));
                 $out->elementEnd("span");
                $out->elementEnd('a');
               //common_local_url('showpendinggradesgroup',array( 'nickname' => "ssii")), 
               //           'Pending Grades', 'Pending notices to grade', false, 'nav_gradereport');
$out->elementEnd('div');
              }

            $out->elementStart('div', array("id" => "entity_graders", "class" => "section"));
            $out->elementStart('h2');

	  // TRANS: text describing the role name of the start aside group profile
            $out->text(_m('Professors'));

            $out->text(' ');

            $graders = Gradesgroup::getGraders($group->id);

            $out->text($graders->N);

            $out->elementEnd('h2');

            $out->elementStart('div', array('id' => 'div-members-scroll'));

            $gmml = new GroupMembersMiniList($graders, $out);
            $cnt = $gmml->show();
            if ($cnt == 0) {
                // TRANS: Description for mini list of group members on a group page when the group has no members.
                $out->element('p', null, _('(None)'));
            }

            $out->elementEnd('div');

            $out->elementEnd('div');
        }

        return true;
    }

    function onStartShowNoticeOptionItems($item) {
        $user = common_current_user();
        
        if (!empty($user)) {

            if ($user->hasRole('grader')) {
              
                // Si la noticia NO es de un profesor, entonces se puede puntuar.
                if (!$item->notice->getProfile()->getUser()->hasRole('grader')) {
   
                    $noticeid = $item->notice->id;
                    $nickname = $user->nickname;
                    $userid = $user->id;

                    // Si puede puntuar (porque es grader en el grupo del tweet)
                    if (Grades::getValidGrader($noticeid, $userid)) {

                        $gradevalue = Grades::getNoticeGrade($noticeid, $nickname);

                        if ($gradevalue != '?') {
                             // TRANS: title and text content of the link for modifying a grade
                             $item->out->elementStart('a', array('href' => 'javascript:editarNota(' . $noticeid . ');', 'class' => 'notice-modify-grade', 'id' => 'button-modify-grade-' . $noticeid, 'title' => _('Modify score')));
                              // TRANS: title and text content of the link for modifying a grade
                            $item->out->raw(_('Modify score'));
                            $item->out->elementEnd('a');
                        }
                    }
                }
            }
        }

        return true;
    }

    function onStartShowNoticeItem($args) {
        $user = common_current_user();
        // Si la noticia es de un profesor, mostramos el birrete.
        if ($args->notice->getProfile()->getUser()->hasRole('grader')) {

            $path = $this->path('css/birrete-small.png');
            $args->out->element('img', array('id' => 'birrete-grades', 'alt' => 'Profesor', 'src' => $path));
        } else {
            $noticeid = $args->notice->id;

            $gradesAndGraders = Grades::getNoticeGradesAndGraders($noticeid);

            $gradeResult = Grades::devolverGrade($gradesAndGraders);

            if (is_array($gradeResult)) {

                $gradeValue = reset($gradeResult);
                $grader = key($gradeResult);

                if ($args->notice->getProfile()->getUser()->id == $user->id || $user->hasRole('grader')) {

                    // Si hay más de una puntuación para el tweet, añadimos JavaScript.
                    if ($grader == "Nota") {

                        $args->out->elementStart('div', array('class' => 'div-with-grades'));
                        foreach ($gradesAndGraders as $profesor => $nota) {

                            $args->out->elementStart('p');
                            $args->out->raw($profesor . ': ' . $nota);
                            $args->out->elementEnd('p');
                        }

                        $args->out->elementEnd('div');

                        $args->out->elementStart('div', array('class' => 'notice-current-grade', 'onclick' => 'mostrarPuntuacion(' . $noticeid . ');'));
                    } else {
                        $args->out->elementStart('div', array('class' => 'notice-current-grade'));
                    }

                    $args->out->elementStart('p', array('class' => 'notice-current-grade-value', 'title' => $grader));

                    if (get_class($args) === 'ThreadedNoticeListSubItem') {
                        $args->out->raw($gradeValue);
                    } else {
                        $args->out->raw($grader . '<br/>' . $gradeValue);
                    }
                    $args->out->elementEnd('p');
                    $args->out->elementEnd('div');
                } else {

                    if (intval($gradeValue) > 2) {
                        $args->out->elementStart('div', array('class' => 'notice-current-grade'));
                        $args->out->elementStart('p', array('class' => 'notice-current-grade-value', 'title' => $grader));
                        $args->out->element('img', array('id' => 'good-answer', 'alt' => 'good answer', 'src' => "https://openclipart.org/image/40px/svg_to_png/3972/PeterM-Medallion.png"));
                        $args->out->elementEnd('p');
                        $args->out->elementEnd('div');
                    }
                }
            }
        }

        return true;
    }

    function onEndShowNoticeItem($args) {

        $user = common_current_user();
        if (!empty($user)) {
              
            if ($user->hasRole('grader') && 
                    strcasecmp($args->notice->source,"activity")!=0) {
  
                // If the notice is not from a professor, then it can be scored
                if (!$args->notice->getProfile()->getUser()->hasRole('grader')) {
  
                    $noticeid = $args->notice->id;
                    $nickname = $user->nickname;
                    $userid = $user->id;


                    // the user can score the notice because it is a valid grader in the group
                    if (Grades::getValidGrader($noticeid, $userid)) {

                        $gradevalue = Grades::getNoticeGrade($noticeid, $nickname);

                        if ($gradevalue == '?')
                            $args->out->elementStart('div', array('class' => 'notice-grades'));


                        else if ($gradevalue != '?')
                            $args->out->elementStart('div', array('id' => 'div-grades-hidden-' . $noticeid, 'class' => 'notice-grades-hidden'));
                        $args->out->elementStart('div', array('id' => 'div-grades-title-' . $noticeid, 'class' => 'div-grades-title'));
                         
                        // TRANS: Is the student answer right?
                        $args->out->text(_m("Is it correct?"));
                        $args->out->elementEnd("div");
                        $args->out->elementStart("ul");

                        $args->out->elementStart("li");
                        $this->showNumbers($args, 1);
                        $args->out->elementEnd("li");

                        $args->out->elementStart("li");
                        $this->showNumbers($args, 2);
                        $args->out->elementEnd("li");

                        $args->out->elementStart("li");
                        $this->showNumbers($args, 3);
                        $args->out->elementEnd("li");

                        $args->out->elementStart("li");
                        $this->showNumbers($args, 4);
                        $args->out->elementEnd("li");

                        $args->out->elementEnd("ul");
                        $args->out->elementEnd('div');
                    }
                }
            }
        }

        return true;
    }

    function onStartShowAccountProfileBlock($out, $profile) {

        if ($profile->getUser()->hasRole('grader')) {

            // Ponemos la imagen del birrete
            $path = $this->path('css/birrete-small.png');
            $out->element('img', array('id' => 'birrete-profile', 'alt' => 'Profesor', 'src' => $path));


            $out->elementStart('p', array('id' => 'label-profesor'));
            // TRANS: Label for Professor on the right side of the screen
            $out->raw(_m('PROFESSOR'));
            $out->elementEnd('p');
        }

        return true;
    }

    function onEndGroupSave($group) {
        $user = common_current_user();
        if (!empty($user)) {
            if ($user->hasRole('grader')) {
                Gradesgroup::vincularGrupo($user->id, $group->id);
            }
        }
    }

    function onEndProfileListItemActionElements($item) {

        if ($item->action->args['action'] === 'groupmembers') {

            $user = common_current_user();

            if ($user->hasRole('grader') && $user->isAdmin($item->group)) {

                if ($user->id != $item->profile->id && !Gradesgroup::isGrader($item->profile->id, $item->group->id)) {

                    $args = array('action' => 'groupmembers',
                        'nickname' => $item->group->nickname);
                    $page = $item->out->arg('page');
                    if ($page) {
                        $args['param-page'] = $page;
                    }

                    $item->out->elementStart('li', 'entity_make_grader');
                    $mgf = new MakeGraderForm($item->out, $item->profile, $item->group, $args);
                    $mgf->show();
                    $item->out->elementEnd('li');
                }
            }
        }



        return true;
    }

    function onEndShowStyles($action) {
        $action->cssLink($this->path('css/grades.css'));
        return true;
    }

    function onEndShowScripts($action) {
        $action->script($this->path('js/grades.js'));
        return true;
    }

}
