License
-------

This software is distributed under the [Affero GPL v3 license](https://www.gnu.org/licenses/agpl-3.0.en.html)

Install instructions
--------------------
Copy this project to the local/plugins folder of your GNU-SOCIAL folder. Please, ensure the project folder is named Grades (with capital lettter)

Then, go to your GNU-SOCIAL instance config.php file and add at the end of the file:

`addPlugin('Grades')` 

Make sure the plugin [ToolsNav](https://git.gnu.io/bolotweet/ToolsLocalNav) is already installed


Usage instructions
---------------
You have a manual and recommendations for scoring [at our site](http://grasiagroup.fdi.ucm.es/bolotweet/en/material)

It is important you follow the instructions to declare the roles of the participants. In order to evaluate, you need to play the "grader" role. Instructions to add users and associate roles are included  in the [administration manual](https://docs.google.com/document/d/13AZTOswHmlYZQ-0S32ZD-H-OBFuDGvcIVL2lcF7RCrs/edit?usp=sharing)

In general, the process is:

-  tell students to write something (usually, you tell them to write something they have learnt)
-  Professors evaluate microannotations (our measurements indicate an average speed of 4 microannotations reviewed per minute)
-  Students can see their own scores (but not others'). Also, they can check the ranking they occupy in the class

Other formulas are possible, though. 

To make your work easier
------------------------

If you want to reduce the effort of tasking students with microannotation activities, please, check the [Task plugin](https://git.gnu.io/bolotweet/Task)


If you want to create reports and compile students microannotations based on hashtags, date, or scored, please, check the [NotesPDF plugin](https://git.gnu.io/bolotweet/NotesPDF)

  
Credits
-------

Jorge J. Gómez Sanz is the head of the project and responsible of initial versions of plugin Grades.

Alvaro Ortego (inactive) was responsible for major plugins Grades, Tasks, NotesPDF, and Logo

Andreea  Ionela is supporting the teaching method, the dissemination of the project, and elaborating teaching material.

The project is also possible because of the collaboration of professors that use the tool. They are listed in the project site

