/**
 * 
 *  Bolotweet-Grades
    Copyright (C) 2018  bolotweet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * Based on a development from Jorge J. Gomez-Sanz
 * @author   Alvaro Ortego <alvorteg@ucm.es>
 *
 */

function editarNota(id) {

    $("#div-grades-hidden-" + id).attr('class', 'notice-grades');

    $("#button-modify-grade-" + id).attr('class', 'notice-modify-grade-hidden');
}

function puntuarNota(noticeid, value,message) {

    $("#notice-" + noticeid + '>.notice-grades').attr('class', 'notice-grades-hidden');

    $("#notice-" + noticeid).append('<p class="temp-text-grades">'+message+': ' + value + '</p>');

}

function mostrarPuntuacion(noticeid) {


    if ($("#notice-" + noticeid + '>.div-with-grades-hidden').length) {

        $("#notice-" + noticeid + '>.div-with-grades-hidden').attr('class', 'div-with-grades');
    }

    else {

        $("#notice-" + noticeid + '>.div-with-grades').attr('class', 'div-with-grades-hidden');

    }
}

function mostrarReport(groupid,textexpand,texthide) {


    if ($("#grade-report-group-" + groupid + '>.grade-show-report').text() == textexpand) {

        $("#grade-report-group-" + groupid + '>.report-group-hidden').toggle('fade', 300);
        $("#grade-report-group-" + groupid + '>.grade-show-report').text(texthide);
    }

    else {


        $("#grade-report-group-" + groupid + '>.report-group-hidden').toggle('fade', 300);
        $("#grade-report-group-" + groupid + '>.grade-show-report').text(textexpand);

    }
}