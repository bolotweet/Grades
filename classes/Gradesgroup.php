<?php

/**
 * 
 *  Bolotweet-Grades
    Copyright (C) 2018  bolotweet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * Based on a development from Jorge J. Gomez-Sanz
 * and a template by Evan Prodromou
 *
 * @author   Alvaro Ortego <alvorteg@ucm.es>
 * @license  http://www.fsf.org/licensing/licenses/agpl.html AGPLv3
 *
 */
if (!defined('STATUSNET') && !defined('LACONICA')) {
    exit(1);
}

class Gradesgroup extends Managed_DataObject {

    /**
     * Notice to favor
     */
    public $__table = 'grades_group';
    public $groupid = null; // group id
    public $userid = null; // user id

   static function staticGet($class="Gradesgroup",$k, $v = null) {
       
          return Gradesgroup::getKV( $k, $v);
       
    }

   


    /**
     * Data definition for email reminders
     */
    public static function schemaDef() {
        return array(
            'description' => 'Context Graders',
            'fields' => array(
                'userid' => array(
                    'type' => 'int',
                    'not null' => true,
                    'description' => 'ID of user'
                ),
                'groupid' => array(
                    'type' => 'int',
                    'not null' => true,
                    'description' => 'Group ID'
                ),
            ),
            'primary key' => array('userid', 'groupid'),
        );
    }
    
    static function getPending( $groupid){
        $qry = 'select count(notices.id) as pendingnotices from (select * from notice where not notice.id in (select notice.id from notice,grades where notice.id=grades.noticeid)  and not notice.id in (select notice.id from notice,profile where profile.id=notice.profile_id  and  (profile.nickname in (select user.nickname from grades_group,user where user.id=grades_group.userid)))) as notices, local_group, group_inbox where notices.id=group_inbox.notice_id and group_inbox.group_id=local_group.group_id and notices.source="web" and local_group.nickname="' . $groupid.'"';

        $graders = new Profile();

        $graders->query($qry);
        $graders->fetch();

        $result=$graders->pendingnotices;
        
        return $result;
    }

    static function vincularGrupo($userid, $groupid) {

        // MAGICALLY put fields into current scope

        $grGroup = new Gradesgroup();

        $grGroup->userid = $userid;
        $grGroup->groupid = $groupid;

        $result = $grGroup->insert();

        if (!$result) {
            common_log_db_error($userid, 'INSERT', __FILE__);
            return false;
        }

        return $grGroup;
    }

    static function desvincularGrupo($userid, $groupid) {


        $grGroup = new Gradesgroup();

        if (common_config('db', 'quote_identifiers'))
            $user_table = '"grades_group"';
        else
            $user_table = 'grades_group';

        $qry = 'DELETE FROM ' . $user_table .
                ' WHERE userid=' . $userid .
                ' AND groupid=' . $groupid;

        $grGroup->query($qry);

        $grGroup->free();
    }

    static function getGraders($groupid) {

        $qry = 'SELECT profile.* ' .
                'FROM profile JOIN grades_group ' .
                'ON profile.id = grades_group.userid ' .
                'WHERE grades_group.groupid = ' . $groupid;

        $graders = new Profile();

        $graders->query($qry);

        return $graders;
    }

    static function getGroups($graderid) {

        $qry = 'SELECT gg.groupid'
                . ' FROM grades_group gg'
                . ' where gg.userid=' . $graderid;

        $grGroup = new Gradesgroup();

        $grGroup->query($qry);

        $groups = array();

        while ($grGroup->fetch()) {
            $groups[] = $grGroup->groupid;
        }

        $grGroup->free();

        return $groups;
    }

    static function isGrader($userid, $groupid) {

        $qry = 'SELECT gg.userid'
                . ' FROM grades_group gg'
                . ' where gg.userid=' . $userid
                . ' and gg.groupid =' . $groupid;

        $grGroup = new Gradesgroup();

        $grGroup->query($qry);


        if ($grGroup->fetch()) {
            $result = true;
        } else {
            $result = false;
        }

        $grGroup->free();
        return $result;
    }

}
