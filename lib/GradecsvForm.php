<?php

/**
 * 
 * BoloTweet 2.0.Based on a development from Jorge J. Gomez-Sanz
 * and a template by Evan Prodromou
 * 
 *
 * @author   Alvaro Ortego <alvorteg@ucm.es>
 * @license  http://www.fsf.org/licensing/licenses/agpl.html AGPLv3
 *
 */
if (!defined('STATUSNET') && !defined('LACONICA')) {
    exit(1);
}

require_once INSTALLDIR . '/lib/form.php';

class GradecsvForm extends Form {

    var $groupid = null;

    /**
     * Constructor
     *
     * @param HTMLOutputter $out    output channel
     * @param Notice        $notice notice to favor
     */
    function __construct($out = null, $groupid = null) {
        parent::__construct($out);

        $this->groupid = $groupid;
    }

    /**
     * Action of the form
     *
     * @return string URL of the action
     */
    function action() {
        return common_local_url('gradeexportcsv');
    }

    /**
     * Data elements
     *
     * @return void
     */
    function formData() {
        $this->out->hidden(null, $this->groupid, 'groupid');
        // TRANS: add the separator among words that you desire to be included in the csv file
        $this->out->element('p', 'options-text-csv', _m('Add the word separator:'));
        $this->out->element('input', array('type' => 'text',
            'name' => 'grade-export-separator',
            'id' => 'grade-export-separator',
            'maxlength' => '1',
            'value' => ','));
        // TRANS: add the delimiter among words that you desire to be included in the csv file
        $this->out->element('p', 'options-text-csv', _m('Add the word delimiter:'));
        $this->out->element('input', array('type' => 'text',
            'name' => 'grade-export-delimiter',
            'id' => 'grade-export-delimiter',
            'maxlength' => '1',
            'value' => '"'));
        // TRANS: inform that only necessary fields will be included
        $this->out->element('p', 'hint-csv-text', _m('Only necessary fields will be included.'));


        $this->out->element('br');
        $this->out->element('br');
        $this->out->element('input', array('type' => 'submit',
            'id' => 'grade-export-button',
              // TRANS: Generate CSV
            'value' => _('Generate CSV'),
              // TRANS: Press to generate the CSV list
            'title' => _('Press to generate the CSV list')));
    }

    /**
     * Class of the form.
     *
     * @return string the form's class
     */
    function formClass() {
        return 'form_export_csv';
    }

}
